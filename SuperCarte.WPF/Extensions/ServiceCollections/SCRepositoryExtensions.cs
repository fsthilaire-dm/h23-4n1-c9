﻿using Microsoft.Extensions.DependencyInjection;
using SuperCarte.Core.Repositories;

namespace SuperCarte.WPF.Extensions.ServiceCollections;

/// <summary>
/// Classe d'extension qui permet d'enregistrer les classes de la catégorie Repository
/// </summary>
public static class SCRepositoryExtensions
{
    /// <summary>
    /// Méthode qui permet d'enregistrer les repositories de l'application
    /// </summary>
    /// <param name="services">La collection de services</param>
    public static void EnregistrerRepositories(this IServiceCollection services)
    {
        services.AddScoped<IRoleRepo, RoleRepo>();
        services.AddScoped<IEnsembleRepo, EnsembleRepo>();
        services.AddScoped<ICategorieRepo, CategorieRepo>();
        services.AddScoped<IUtilisateurRepo, UtilisateurRepo>();
        services.AddScoped<ICarteRepo, CarteRepo>();
        services.AddScoped<IUtilisateurCarteRepo, UtilisateurCarteRepo>();
    }
}

