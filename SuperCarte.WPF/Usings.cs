﻿global using SuperCarte.WPF; //Les classes à la racine de l'application WPF
global using SuperCarte.EF.Data; //Les classes du modèle du contexte
global using SuperCarte.EF.Data.Context; // La classe du contexte
global using System;
global using System.Collections.Generic;
global using System.Threading.Tasks;