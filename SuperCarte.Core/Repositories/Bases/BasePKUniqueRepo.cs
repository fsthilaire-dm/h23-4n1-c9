﻿using SuperCarte.EF.Data.Context;

namespace SuperCarte.Core.Repositories.Bases;

// <summary>
/// Classe abstraite générique qui contient les opérations de base des tables de la base de données pour une table à clé primaire unique
/// </summary>
/// <typeparam name="TData">Type du modèle de données / table</typeparam>
/// <typeparam name="TClePrimaire">Type de la clé primaire</typeparam>
public abstract class BasePKUniqueRepo<TData, TClePrimaire> : BaseRepo<TData>, IBasePKUniqueRepo<TData, TClePrimaire> where TData : class
{
    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="bd">Contexte de la base de données</param>
    public BasePKUniqueRepo(SuperCarteContext bd) : base(bd)
    {
        //Vide, il sert uniquement a recevoir le contexte et à l'envoyer à la classe parent.
    }

    public async Task<TData?> ObtenirParCleAsync(TClePrimaire clePrimaire)
    {
        return await _bd.FindAsync<TData>(clePrimaire);
    }

    public TData? ObtenirParCle(TClePrimaire clePrimaire)
    {
        return _bd.Find<TData>(clePrimaire);
    }

    public async Task SupprimerParCleAsync(TClePrimaire clePrimaire, bool enregistrer)
    {
        TData? item = await ObtenirParCleAsync(clePrimaire);

        //Vérifie si un item a été trouvé avec la clé spécifiée
        if (item != null)
        {
            //Il y a un item avec la clé spécifiée
            await SupprimerAsync(item, enregistrer);
        }
        else
        {
            //Il n'y a pas d'item avec la clé spécifiée
            throw new Exception("Impossible de trouver l'item à supprimer avec la clé spécifiée.");
        }
    }

    public void SupprimerParCle(TClePrimaire clePrimaire, bool enregistrer)
    {
        TData? item = ObtenirParCle(clePrimaire);

        //Vérifie si un item a été trouvé avec la clé spécifiée
        if (item != null)
        {
            //Il y a un item avec la clé spécifiée
            Supprimer(item, enregistrer);
        }
        else
        {
            //Il n'y a pas d'item avec la clé spécifiée
            throw new Exception("Impossible de trouver l'item à supprimer avec la clé spécifiée.");
        }
    }
}
