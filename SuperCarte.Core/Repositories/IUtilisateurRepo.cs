﻿using SuperCarte.Core.Repositories.Bases;
using SuperCarte.EF.Data;

namespace SuperCarte.Core.Repositories;

/// <summary>
/// Interface qui contient les méthodes de communication avec la base de données pour la table Utilisateur
/// </summary>
public interface IUtilisateurRepo : IBasePKUniqueRepo<Utilisateur, int>
{
}
