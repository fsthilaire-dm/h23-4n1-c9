﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SuperCarte.EF.Migrations
{
    /// <inheritdoc />
    public partial class FKOnDeleteNoAction : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Carte_Categorie_CategorieId",
                table: "Carte");

            migrationBuilder.DropForeignKey(
                name: "FK_Carte_Ensemble_EnsembleId",
                table: "Carte");

            migrationBuilder.DropForeignKey(
                name: "FK_Utilisateur_Role_RoleId",
                table: "Utilisateur");

            migrationBuilder.DropForeignKey(
                name: "FK_UtilisateurCarte_Carte_CarteId",
                table: "UtilisateurCarte");

            migrationBuilder.DropForeignKey(
                name: "FK_UtilisateurCarte_Utilisateur_UtilisateurId",
                table: "UtilisateurCarte");

            migrationBuilder.DropIndex(
                name: "IX_UtilisateurCarte_CarteId",
                table: "UtilisateurCarte");

            migrationBuilder.AddForeignKey(
                name: "FK_Carte_Categorie_CategorieId",
                table: "Carte",
                column: "CategorieId",
                principalTable: "Categorie",
                principalColumn: "CategorieId");

            migrationBuilder.AddForeignKey(
                name: "FK_Carte_Ensemble_EnsembleId",
                table: "Carte",
                column: "EnsembleId",
                principalTable: "Ensemble",
                principalColumn: "EnsembleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Utilisateur_Role_RoleId",
                table: "Utilisateur",
                column: "RoleId",
                principalTable: "Role",
                principalColumn: "RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_UtilisateurCarte_Carte_CarteId",
                table: "UtilisateurCarte",
                column: "CarteId",
                principalTable: "Carte",
                principalColumn: "CarteId");

            migrationBuilder.AddForeignKey(
                name: "FK_UtilisateurCarte_Utilisateur_UtilisateurId",
                table: "UtilisateurCarte",
                column: "UtilisateurId",
                principalTable: "Utilisateur",
                principalColumn: "UtilisateurId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Carte_Categorie_CategorieId",
                table: "Carte");

            migrationBuilder.DropForeignKey(
                name: "FK_Carte_Ensemble_EnsembleId",
                table: "Carte");

            migrationBuilder.DropForeignKey(
                name: "FK_Utilisateur_Role_RoleId",
                table: "Utilisateur");

            migrationBuilder.DropForeignKey(
                name: "FK_UtilisateurCarte_Carte_CarteId",
                table: "UtilisateurCarte");

            migrationBuilder.DropForeignKey(
                name: "FK_UtilisateurCarte_Utilisateur_UtilisateurId",
                table: "UtilisateurCarte");

            migrationBuilder.CreateIndex(
                name: "IX_UtilisateurCarte_CarteId",
                table: "UtilisateurCarte",
                column: "CarteId");

            migrationBuilder.AddForeignKey(
                name: "FK_Carte_Categorie_CategorieId",
                table: "Carte",
                column: "CategorieId",
                principalTable: "Categorie",
                principalColumn: "CategorieId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Carte_Ensemble_EnsembleId",
                table: "Carte",
                column: "EnsembleId",
                principalTable: "Ensemble",
                principalColumn: "EnsembleId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Utilisateur_Role_RoleId",
                table: "Utilisateur",
                column: "RoleId",
                principalTable: "Role",
                principalColumn: "RoleId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UtilisateurCarte_Carte_CarteId",
                table: "UtilisateurCarte",
                column: "CarteId",
                principalTable: "Carte",
                principalColumn: "CarteId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UtilisateurCarte_Utilisateur_UtilisateurId",
                table: "UtilisateurCarte",
                column: "UtilisateurId",
                principalTable: "Utilisateur",
                principalColumn: "UtilisateurId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
