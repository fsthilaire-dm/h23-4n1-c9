﻿namespace SuperCarte.EF.Data;

public class Ensemble
{
    public int EnsembleId { get; set; }

    public string Nom { get; set; } = null!;

    public DateTime Disponibilite { get; set; }

    public ICollection<Carte> CarteListe { get; set; } = new List<Carte>();
}