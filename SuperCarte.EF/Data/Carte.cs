﻿namespace SuperCarte.EF.Data;

public class Carte
{
    public int CarteId { get; set; }

    public string Nom { get; set; } = null!;

    public byte[]? Image { get; set; }

    public short Vie { get; set; }

    public short Armure { get; set; }

    public short Attaque { get; set; }

    public bool EstRare { get; set; }

    public decimal PrixRevente { get; set; }

    public int CategorieId { get; set; }

    public int EnsembleId { get; set; }

    public Categorie Categorie { get; set; } = null!;

    public Ensemble Ensemble { get; set; } = null!;

    public ICollection<UtilisateurCarte> UtilisateurCarteListe { get; set; } = new List<UtilisateurCarte>();
}