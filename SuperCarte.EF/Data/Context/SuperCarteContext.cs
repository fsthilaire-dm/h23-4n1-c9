﻿using Microsoft.EntityFrameworkCore;
using System.Reflection.Emit;

namespace SuperCarte.EF.Data.Context;

/// <summary>
/// Contexte pour la base de de données SuperCarte
/// </summary>
public class SuperCarteContext : DbContext
{
    private bool _executerSeed = false;

    /// <summary>
    /// Constructeur pour la migration
    /// </summary>
	public SuperCarteContext() : base()
    {

    }

    /// <summary>
    /// Constructeur pour l'utilisation en programme
    /// </summary>
    /// <param name="options">Option de la base de données</param>
    public SuperCarteContext(DbContextOptions<SuperCarteContext> options)
        : base(options)
    {
    }

#if DEBUG //Permet d'inclure cette méthode uniquement si l'application est en mode DEBUG
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {        
        //Vérifie si la configuration n'a pas été spécifiée par un fichier de configuration
        if (optionsBuilder.IsConfigured == false)
        {
            //Aucune configuration à partir d'un fichier de configuration
            //Option de base pour la migration            
            string? chaineConnexion = Environment.GetEnvironmentVariable("MIGRATION_CONNECTION_STRING");

            //Vérifie si la variable n'est pas vide
            if (string.IsNullOrEmpty(chaineConnexion) == false)
            {
                //La variable n'est pas vide, la chaine de connexion est appliquée
                optionsBuilder.UseSqlServer(chaineConnexion);

                _executerSeed = true;
            }
            else
            {
                //Il n'y a aucune chaine de connexion.
                throw new Exception("La variable MIGRATION_CONNECTION_STRING n'est pas spécifiée. Effectuez la commande suivante dans la Console du Gestionnaire de package : $env:MIGRATION_CONNECTION_STRING=\"[ma chaine de connexion]\" ");
            }
        }
    }
#endif

    /// <summary>
    /// Configuration spécifique de la base de données
    /// </summary>
    /// <param name="modelBuilder"></param>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        //Table Role
        modelBuilder.Entity<Role>(entity =>
        {
            //Spécifie le nom de la table dans la BD
            entity.ToTable("Role");

            entity.Property(t => t.Nom)
                .IsUnicode(false) //VARCHAR ou CHAR
                .HasMaxLength(25); //VARCHAR(25)   
        });

        //Table Utilisateur
        modelBuilder.Entity<Utilisateur>(entity =>
        {
            //Spécifie le nom de la table dans la BD
            entity.ToTable("Utilisateur");

            entity.Property(t => t.Prenom)
                .IsUnicode(false)
                .HasMaxLength(50);

            entity.Property(t => t.Nom)
                .IsUnicode(false)
                .HasMaxLength(50);

            entity.Property(t => t.NomUtilisateur)
                .IsUnicode(false)
                .HasMaxLength(50);

            entity.Property(t => t.MotPasseHash)
                .IsUnicode(false)
                .IsFixedLength(true) //CHAR
                .HasMaxLength(60);

            entity.HasIndex(t => t.NomUtilisateur).IsUnique(true);

            entity.HasOne(t => t.Role).WithMany(p => p.UtilisateurListe)
                .HasForeignKey(t => t.RoleId)
                .OnDelete(DeleteBehavior.ClientSetNull);
        });

        //Table Categorie
        modelBuilder.Entity<Categorie>(entity =>
        {
            //Spécifie le nom de la table dans la BD
            entity.ToTable("Categorie");

            entity.Property(t => t.Nom)
                    .IsUnicode(false)
                    .HasMaxLength(35);

            entity.Property(t => t.Description)
                    .IsUnicode(false)
                    .HasMaxLength(50);
        });

        //Table Ensemble
        modelBuilder.Entity<Ensemble>(entity =>
        {
            //Spécifie le nom de la table dans la BD
            entity.ToTable("Ensemble");

            entity.Property(t => t.Nom)
                    .IsUnicode(false)
                    .HasMaxLength(50);

            entity.Property(t => t.Disponibilite)
                .HasColumnType("DATE");
        });

        //Table Carte
        modelBuilder.Entity<Carte>(entity =>
        {
            //Spécifie le nom de la table dans la BD
            entity.ToTable("Carte");

            entity.Property(t => t.Nom)
                    .IsUnicode(false)
                    .HasMaxLength(100);

            entity.Property(t => t.PrixRevente)
                .HasPrecision(8,2);

            entity.HasOne(t => t.Ensemble).WithMany(p => p.CarteListe)
                    .HasForeignKey(t => t.EnsembleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(t => t.Categorie).WithMany(p => p.CarteListe)
                    .HasForeignKey(t => t.CategorieId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
        });

        //Table UtilisateurCarte
        modelBuilder.Entity<UtilisateurCarte>(entity =>
        {
            //Spécifie le nom de la table dans la BD
            entity.ToTable("UtilisateurCarte");

            //Spécifie la clé primaire
            entity.HasKey(t => new { t.UtilisateurId, t.CarteId });

            entity.HasOne(t => t.Utilisateur).WithMany(p => p.UtilisateurCarteListe)
                    .HasForeignKey(t => t.UtilisateurId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

            entity.HasOne(t => t.Carte).WithMany(p => p.UtilisateurCarteListe)
                    .HasForeignKey(t => t.CarteId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
        });

        if(_executerSeed == true)
        {
            Seed(modelBuilder);
        }
    }

    /// <summary>
    /// Méthode qui s'occupe de la création des données
    /// </summary>
    private void Seed(ModelBuilder modelBuilder)
    {
        //Les données à ajouter
        Role[] roles = 
        {
            new Role() 
            { 
                RoleId = 1,
                Nom = "Administrateur"                
            },
            new Role()
            {
                RoleId = 2,
                Nom = "Utilisateur"
            },
        };

        Utilisateur[] utilisateurs =
        {
            new Utilisateur()
            {
                UtilisateurId = 1,
                Prenom = "François",
                Nom = "St-Hilaire",
                NomUtilisateur = "fsthilaire",
                MotPasseHash = "$2y$11$IY6NG9FkTSI1dnjLfSbuOuNkuyI7IZHxHSOD5Td6AlwvroUz/vzLK", //Native3! avec Bcrypt
                RoleId = 1 //Admin
            },
            new Utilisateur()
            {
                UtilisateurId = 2,
                Prenom = "Benoit",
                Nom = "Tremblay",
                NomUtilisateur = "btremblay",
                MotPasseHash = "$2y$11$ewK3YsMGQ1IMKEzJUAjyVe0P19I0gEbTO998mwfVbSSA8nZ6MG/ha", //Web4MVC! avec Bcrypt
                RoleId = 2 //Utilisateur
            },
            new Utilisateur() 
            {
                UtilisateurId = 3,
                Prenom = "Tony",
                Nom = "Stark",
                NomUtilisateur = "tstark",
                MotPasseHash = "$2y$11$VfcNowkWResPQKl0AA3MJ.w1LXBqmMM77YKlyf32Glr9TWG4xxyD2", //#NotAdmin! avec Bcrypt
                RoleId = 2 //Utilisateur
            }
        };

        //Ajout dans les tables
        modelBuilder.Entity<Role>().HasData(roles);
        modelBuilder.Entity<Utilisateur>().HasData(utilisateurs);
    }

    public DbSet<Role> RoleTb { get; set; }

    public DbSet<Utilisateur> UtilisateurTb { get; set; }

    public DbSet<Categorie> CategorieTb { get; set; }

    public DbSet<Ensemble> EnsembleTb { get; set; }

    public DbSet<Carte> CarteTb { get; set; }

    public DbSet<UtilisateurCarte> UtilisateurCarteTb { get; set; }
}